package com.feizhaiyou.encrypt.constants;

/**
 * @author ls
 * @since 2023-07-27
 */
public class SensitiveType {
    /**
     * 中文名
     */
    public static final String CHINESE_NAME = "CHINESE_NAME";

    /**
     * 身份证号
     */
    public static final String ID_CARD = "ID_CARD";

    /**
     * 座机号
     */
    public static final String FIXED_PHONE = "FIXED_PHONE";

    /**
     * 手机号
     */
    public static final String MOBILE_PHONE = "MOBILE_PHONE";

    /**
     * 地址
     */
    public static final String ADDRESS = "ADDRESS";

    /**
     * 电子邮件
     */
    public static final String EMAIL = "EMAIL";

    /**
     * 银行卡
     */
    public static final String BANK_CARD = "BANK_CARD";

    /**
     * 密码
     */
    public static final String PASSWORD = "PASSWORD";

    /**
     * 车牌号
     */
    public static final String CARNUMBER = "CARNUMBER";

    /**
     * 默认
     */
    public static final String DEFAULT = "DEFAULT";
}
