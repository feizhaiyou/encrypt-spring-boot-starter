package com.feizhaiyou.encrypt.constants;

/**
 * @author ls
 */
public enum SecurityMode {

    BASE64,

    HEX;
}
