package com.feizhaiyou.encrypt.format;

/**
 * @author ls
 * @since 2023-07-27
 */
public interface SensitiveProcessor {

    /**
     * 脱敏处理
     * @param text 敏感信息
     * @param type 脱敏类型标识
     * @return 脱敏后的信息
     */
    String format(String text, String type);
}
