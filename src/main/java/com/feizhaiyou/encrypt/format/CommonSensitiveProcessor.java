package com.feizhaiyou.encrypt.format;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.StringUtils;

import static com.feizhaiyou.encrypt.constants.SensitiveType.*;

/**
 * @author ls
 * @since 2023-07-27
 */
public class CommonSensitiveProcessor implements SensitiveProcessor {

    @Override
    public String format(String text, String type) {
        if (StringUtils.isBlank(text)) {
            return text;
        }
        switch (type) {
            case CHINESE_NAME:
                return chineseName(text);
            case ID_CARD:
                return idCardNum(text);
            case FIXED_PHONE:
                return fixedPhone(text);
            case MOBILE_PHONE:
                return mobilePhone(text);
            case ADDRESS:
                return address(text, 8);
            case EMAIL:
                return email(text);
            case BANK_CARD:
                return bankCard(text);
            case PASSWORD:
                return password(text);
            case CARNUMBER:
                return carNumber(text);
            case DEFAULT:
                return defaultType(text);
            default:
                return text;
        }
    }

    /**
     * 【默认】只显示第一个和最后一个字符，中间替换*，比如：怎*样
     *
     * @param text
     * @return
     */
    public static String defaultType(String text) {
        if (StringUtils.isBlank(text)) {
            return "";
        }
        if (text.length() == 1) {
            return "*";
        }
        if (text.length() == 2) {
            return StringUtils.left(text, 1) + "*";
        }
        if (text.length() >= 3) {
            return StringUtils.rightPad(StringUtils.left(text, 1), StringUtils.length(text) - 1, "*") + StringUtils.right(text, 1);
        }
        return text;
    }

    /**
     * 【中文姓名】只显示第一个汉字，其他隐藏为2个星号，比如：李**
     *
     * @param fullName
     * @return
     */
    private String chineseName(String fullName) {
        if (StrUtil.isBlank(fullName)) {
            return "";
        }
        String name = StringUtils.left(fullName, 1);
        return StringUtils.rightPad(name, StringUtils.length(fullName), "*");
    }

    /**
     * 【身份证号】显示最后四位，其他隐藏。共计18位或者15位，比如：*************1234
     *
     * @param id
     * @return
     */
    private String idCardNum(String id) {
        if (StringUtils.isBlank(id)) {
            return "";
        }
        String num = StringUtils.right(id, 4);
        return StringUtils.leftPad(num, StringUtils.length(id), "*");
    }

    /**
     * 【固定电话 后四位，其他隐藏，比如1234
     *
     * @param num
     * @return
     */
    private String fixedPhone(String num) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        return StringUtils.leftPad(StringUtils.right(num, 4), StringUtils.length(num), "*");
    }

    /**
     * 【手机号码】前三位，后2位，其他隐藏，比如135******10
     *
     * @param num
     * @return
     */
    private String mobilePhone(String num) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        return StringUtils.left(num, 3).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(num, 2), StringUtils.length(num), "*"), "***"));
    }

    /**
     * 【地址】只显示到地区，不显示详细地址，比如：北京市海淀区****
     *
     * @param address
     * @param sensitiveSize 敏感信息长度
     * @return
     */
    private String address(String address, int sensitiveSize) {
        if (StringUtils.isBlank(address)) {
            return "";
        }
        int length = StringUtils.length(address);
        return StringUtils.rightPad(StringUtils.left(address, length - sensitiveSize), length, "*");
    }

    /**
     * 【电子邮箱 邮箱前缀仅显示第一个字母，前缀其他隐藏，用星号代替，@及后面的地址显示，比如：d**@126.com>
     *
     * @param email
     * @return
     */
    private String email(String email) {
        if (StringUtils.isBlank(email)) {
            return "";
        }
        int index = StringUtils.indexOf(email, "@");
        if (index <= 1) {
            return email;
        } else {
            return StringUtils.rightPad(StringUtils.left(email, 1), index, "*").concat(StringUtils.mid(email, index, StringUtils.length(email)));
        }
    }

    /**
     * 【银行卡号】前六位，后四位，其他用星号隐藏每位1个星号，比如：6222600**********1234
     *
     * @param cardNum
     * @return
     */
    private String bankCard(String cardNum) {
        if (StringUtils.isBlank(cardNum)) {
            return "";
        }
        return StringUtils.left(cardNum, 6).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(cardNum, 4), StringUtils.length(cardNum), "*"), "******"));
    }

    /**
     * 【密码】密码的全部字符都用*代替，比如：******
     *
     * @param password
     * @return
     */
    private String password(String password) {
        if (StringUtils.isBlank(password)) {
            return "";
        }
        String pwd = StringUtils.left(password, 0);
        return StringUtils.rightPad(pwd, StringUtils.length(password), "*");
    }

    /**
     * 【车牌号】前两位后一位，比如：苏M****5
     *
     * @param carNumber
     * @return
     */
    private String carNumber(String carNumber) {
        if (StringUtils.isBlank(carNumber)) {
            return "";
        }
        return StringUtils.left(carNumber, 2).
                concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(carNumber, 1), StringUtils.length(carNumber), "*"), "**"));

    }
}
