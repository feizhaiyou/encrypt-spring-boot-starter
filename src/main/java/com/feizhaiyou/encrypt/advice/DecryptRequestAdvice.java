package com.feizhaiyou.encrypt.advice;

import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * @author ls
 */
@Slf4j
public class DecryptRequestAdvice extends AbstractSecurityAdvice {

    public DecryptRequestAdvice(int maxDeep, List<String> classPackage) {
        DEFAULT_CLEAN_DEPTH = maxDeep;
        STANDARD_CLASS = classPackage;
    }

    @Override
    public String handleSecurity(String value, Annotation[] annotations) {
        if (securityHandler != null) {
            Annotation acquire = securityHandler.acquire(annotations);
            // 处理器分优先级，优先级高的处理器优先处理，只要找到一个支持的处理器，后续的处理器不再处理
            if (acquire != null) {
                value = securityHandler.handleDecrypt(value, acquire);
            }
        }
        return value;
    }


    /**
     * 给请求解密
     *
     * @param value 需要解密的对象
     * @return
     * @throws Exception
     */
    public Object decrypt(Object value, Annotation[] annotations) throws Exception {
        return handleObject(0, DEFAULT_CLEAN_DEPTH, value, annotations);
    }

}
