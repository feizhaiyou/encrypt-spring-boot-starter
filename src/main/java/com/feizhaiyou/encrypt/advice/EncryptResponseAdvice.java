package com.feizhaiyou.encrypt.advice;

import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * @author ls
 */
@Slf4j
public class EncryptResponseAdvice extends AbstractSecurityAdvice {

    public EncryptResponseAdvice(int maxDeep, List<String> classPackage) {
        DEFAULT_CLEAN_DEPTH = maxDeep;
        STANDARD_CLASS = classPackage;
    }


    /**
     * 给响应加密
     *
     * @param value 响应的对象
     * @return
     * @throws Exception
     */
    public Object encrypt(Object value, Annotation[] annotations) throws Exception {
        return handleObject(0, DEFAULT_CLEAN_DEPTH, value, annotations);
    }

    @Override
    public String handleSecurity(String value, Annotation[] annotations) {
        // 脱敏
        if (sensitiveHandler != null) {
            Annotation acquire = sensitiveHandler.acquire(annotations);
            if (acquire != null) {
                value = sensitiveHandler.format(value, acquire);
            }
        }
        // 加密
        if (securityHandler != null) {
            Annotation acquire = securityHandler.acquire(annotations);
            if (acquire != null) {
                value = securityHandler.handleEncrypt(value, acquire);
            }
        }
        return value;
    }

}
