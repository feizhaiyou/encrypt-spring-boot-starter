package com.feizhaiyou.encrypt.codec;

/**
 * @author ls
 */
public interface SecurityProcessor {
    /**
     * 加密处理
     *
     * @param data 原始数据
     * @return 加密信息
     */
    byte[] encrypt(byte[] data);

    /**
     * 解密处理
     *
     * @param text 加密数据
     * @return 原始信息
     */
    byte[] decrypt(String text);
}
