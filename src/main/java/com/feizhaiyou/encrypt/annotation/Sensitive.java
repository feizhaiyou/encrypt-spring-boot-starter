package com.feizhaiyou.encrypt.annotation;

import com.feizhaiyou.encrypt.constants.SensitiveType;

import java.lang.annotation.*;

/**
 * @author ls
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Sensitive {

    /**
     * 脱敏数据类型
     *
     * @return
     */
    String type() default SensitiveType.DEFAULT;

    /**
     * 是否脱敏
     *
     * @return
     */
    boolean required() default true;

}
