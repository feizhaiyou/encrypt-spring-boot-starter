package com.feizhaiyou.encrypt.annotation;

import java.lang.annotation.*;

/**
 * @author ls
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Security {

    /**
     * 是否对响应加密
     *
     * @return
     */
    boolean encrypt() default true;

    /**
     * 是否对请求解密
     *
     * @return
     */
    boolean decrypt() default true;


}
