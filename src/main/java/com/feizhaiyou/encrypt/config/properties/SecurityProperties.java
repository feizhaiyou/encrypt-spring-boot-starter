package com.feizhaiyou.encrypt.config.properties;

import com.feizhaiyou.encrypt.constants.SecurityMode;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ls
 * @since 2023-07-21
 */
@ConfigurationProperties(prefix = "fzy.security")
@Data
public class SecurityProperties {

    /**
     * 加密字符串转义方式
     * {@link SecurityMode}
     */
    private SecurityMode mode = SecurityMode.BASE64;

    /**
     * 是否启用
     */
    private boolean enable = true;

    /**
     * 加解密类型
     */
    private String type = "AES";

    /**
     * 加解密转义字符串编码
     */
    private String charset = "UTF-8";

    /**
     * 对称加解密秘钥
     */
    private String secret = "+6cuvzvyrFZpRG9pf3r7eQ==";

    /**
     * 加解密对象字段递归深度
     */
    private int maxDeep = 5;

    /**
     * 非对称加密私钥
     */
    private String privateKey;

    /**
     * 非对称加密公钥
     */
    private String publicKey;

    /**
     * 扫描加解密实体类的包范围
     */
    private List<String> classPackage = new ArrayList<>();
}
