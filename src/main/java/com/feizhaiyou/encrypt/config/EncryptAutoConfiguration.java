package com.feizhaiyou.encrypt.config;

import com.feizhaiyou.encrypt.advice.DecryptRequestAdvice;
import com.feizhaiyou.encrypt.advice.EncryptResponseAdvice;
import com.feizhaiyou.encrypt.annotation.Security;
import com.feizhaiyou.encrypt.annotation.Sensitive;
import com.feizhaiyou.encrypt.aspect.SecurityAspect;
import com.feizhaiyou.encrypt.codec.*;
import com.feizhaiyou.encrypt.config.properties.SecurityProperties;
import com.feizhaiyou.encrypt.format.CommonSensitiveProcessor;
import com.feizhaiyou.encrypt.format.SensitiveProcessor;
import com.feizhaiyou.encrypt.handler.CommonSecurityHandler;
import com.feizhaiyou.encrypt.handler.CommonSensitiveHandler;
import com.feizhaiyou.encrypt.handler.SecurityHandler;
import com.feizhaiyou.encrypt.handler.SensitiveHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author ls
 * @since 2023-07-21
 */
@Configuration
@ConditionalOnWebApplication //web应用生效
@EnableConfigurationProperties({SecurityProperties.class})
@Slf4j
public class EncryptAutoConfiguration {
    /**
     * {@link SecurityProperties}
     */
    private SecurityProperties securityProperties;

    public EncryptAutoConfiguration(SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "fzy.security", name = "type", havingValue = "AES")
    public SecurityProcessor aesProcessor() {
        return new AESProcessor(securityProperties.getSecret());
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "fzy.security", name = "type", havingValue = "RSA")
    public SecurityProcessor rsaProcessor() {
        return new RSAProcessor(securityProperties.getPublicKey(), securityProperties.getPrivateKey());
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "fzy.security", name = "type", havingValue = "SM4")
    public SecurityProcessor sm4Processor() {
        return new SM4Processor(securityProperties.getSecret());
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "fzy.security", name = "type", havingValue = "SM2")
    public SecurityProcessor sm2Processor() {
        return new SM2Processor(securityProperties.getPublicKey(), securityProperties.getPrivateKey());
    }

    @Bean
    @ConditionalOnMissingBean
    public SecurityHandler<Security> commonSecurityHandler(@Autowired SecurityProcessor aesProcessor) {
        log.debug("encrypt-starter loading SecurityProcessor: {}", aesProcessor.getClass().getName());
        return new CommonSecurityHandler(aesProcessor, securityProperties.getMode(), securityProperties.getCharset());
    }

    @Bean
    @ConditionalOnMissingBean
    public SensitiveProcessor commonSensitiveProcessor() {
        return new CommonSensitiveProcessor();
    }

    @Bean
    @ConditionalOnMissingBean
    public SensitiveHandler<Sensitive> commonSensitiveHandler(@Autowired SensitiveProcessor commonSensitiveProcessor) {
        log.debug("encrypt-starter loading SensitiveHandler: {}", commonSensitiveProcessor.getClass().getName());
        return new CommonSensitiveHandler(commonSensitiveProcessor);
    }


    @Bean
    @ConditionalOnMissingBean
    public DecryptRequestAdvice decryptRequestAdvice() {
        return new DecryptRequestAdvice(securityProperties.getMaxDeep(), securityProperties.getClassPackage());
    }

    @Bean
    @ConditionalOnMissingBean
    public EncryptResponseAdvice encryptResponseAdvice() {
        List<String> classPackage = securityProperties.getClassPackage();
        log.debug("encrypt-starter scan class packages: {}", classPackage);
        return new EncryptResponseAdvice(securityProperties.getMaxDeep(), classPackage);
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "fzy.security", name = "enable", havingValue = "true")
    public SecurityAspect securityAspect(@Autowired DecryptRequestAdvice decryptRequestAdvice,
                                     @Autowired EncryptResponseAdvice encryptResponseAdvice) {
        return new SecurityAspect(decryptRequestAdvice, encryptResponseAdvice);
    }


}
