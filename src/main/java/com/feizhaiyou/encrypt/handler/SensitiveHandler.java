package com.feizhaiyou.encrypt.handler;

import java.lang.annotation.Annotation;

/**
 * @author ls
 * @since 2023-07-27
 */
public interface SensitiveHandler<A extends Annotation> {

    /**
     * 返回字段注解
     *
     * @return
     */
    A acquire(Annotation[] annotations);

    /**
     * 脱敏
     *
     * @param source
     * @param annotation
     * @return
     */
    String format(String source, A annotation);
}
