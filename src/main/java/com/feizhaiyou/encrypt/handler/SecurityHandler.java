package com.feizhaiyou.encrypt.handler;


import java.lang.annotation.Annotation;

/**
 * @author ls
 */
public interface SecurityHandler<A extends Annotation> {

    /**
     * 返回字段注解
     *
     * @return
     */
    A acquire(Annotation[] annotations);

    /**
     * 值加密
     *
     * @param source
     * @param annotation
     * @return
     */
    String handleEncrypt(String source, A annotation);

    /**
     * 值解密
     *
     * @param source
     * @param annotation
     * @return
     */
    String handleDecrypt(String source, A annotation);
}
